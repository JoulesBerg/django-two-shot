from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

# Create your views here.
def user_create(request):
    if request.method == "POST":
         form = UserCreationForm(request.POST)
         if form.is_valid():
             user = form.save()
             #(commit=False)?
             login(request,user)
             #User.objects.create_user(user.username,user.email,user.password)?
             return redirect("home")
    else:
         form = UserCreationForm()
    context = {"form": form,}
    return render(request, "registration/signup.html", context)

