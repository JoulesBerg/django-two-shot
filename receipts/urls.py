from django.contrib import admin
from django.urls import path
from .views import (
    AccountCreateView,
    ExpenseCategoryCreateView,
    ExpenseCategoryListView,
    ReceiptCreateView,
    ReceiptListView,
    AccountListView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="list_categories",
    ),
    path("accounts/", AccountListView.as_view(), name="bank_accounts"),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="create_category",
    ),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_account"
    ),
]
