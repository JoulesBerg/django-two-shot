from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required

from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from .models import Receipt, ExpenseCategory, Account


# Create your views here.
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    context_object_name = "receipts"
    template_name = "receipts/rlist.html"
    paginate_by = 10

    def get_context_data(
        self,
    ):
        context = super().get_context_data()
        query = self.request.GET.get("q")
        if not query:
            query = ""
        context["query"] = query
        return context

    def get_queryset(
        self,
    ):

        user = self.request.user
        query = self.request.GET.get("q")
        if not query:
            query = ""
        return Receipt.objects.filter(
            vendor__icontains=query,
            purchaser=user,
        )


class ReceiptCreateView(CreateView):
    model = Receipt
    template_name = "receipts/rcreate.html"
    fields = [
        "vendor",
        "total",
        "tax",
        "date",
        "category",
        "account",
    ]

    def form_valid(self, form):
        form.instance.purchaser = (
            self.request.user
        )  # intercept form to set authoruser automatically

        return super().form_valid(form)

    success_url = reverse_lazy("home")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    context_object_name = "categories"
    template_name = "receipts/eclist.html"
    paginate_by = 10

    def get_context_data(
        self,
    ):
        context = super().get_context_data()
        query = self.request.GET.get("q")
        if not query:
            query = ""
        context["query"] = query
        return context

    def get_queryset(
        self,
    ):

        user = self.request.user
        query = self.request.GET.get("q")
        if not query:
            query = ""
        return ExpenseCategory.objects.filter(
            name__icontains=query,
            owner=user,
        )


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    context_object_name = "accounts"
    template_name = "receipts/alist.html"
    paginate_by = 10

    def get_context_data(
        self,
    ):
        context = super().get_context_data()
        query = self.request.GET.get("q")
        if not query:
            query = ""
        context["query"] = query
        return context

    def get_queryset(
        self,
    ):

        user = self.request.user
        query = self.request.GET.get("q")
        if not query:
            query = ""
        return Account.objects.filter(
            name__icontains=query,
            owner=user,
        )


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/eccreate.html"
    fields = ["name"]

    def form_valid(self, form):
        form.instance.owner = self.request.user

        return super().form_valid(form)

    success_url = reverse_lazy("list_categories")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/acreate.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        form.instance.owner = self.request.user

        return super().form_valid(form)

    success_url = reverse_lazy("bank_accounts")
