from django.contrib import admin
from .models import Account, ExpenseCategory, Receipt

# Register your models here.
admin.site.register(Account)
admin.site.register(ExpenseCategory)
admin.site.register(Receipt)
