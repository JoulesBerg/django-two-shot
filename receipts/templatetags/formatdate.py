from django import template

register = template.Library()


@register.filter
def formatdate(date):
    formatdate = date.strftime("%m-%d-%Y")
    return formatdate
